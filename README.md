# Node Task

This project contains node hello-world

## Installation steps

1. Copy the repo
2. Install dependencies
3. Start the app

## Terminal commands

Code block:

```sh
git clone https://gitlab.com/Nezdera/node-task
cd ./node-task
npm i
```

start cmd: `npm run start`

## App should work

![:)](https://media1.tenor.com/images/2cbe796de7fcdf076e7d581a35254b10/tenor.gif?itemid=15030119)